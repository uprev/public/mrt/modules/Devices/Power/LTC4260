---
name: LTC4260
description: Hot Swap Power Management Controller with I2C Compatible Monitoring
category: Device/Power
created: 12/15/2023
datasheet: https://www.analog.com/media/en/technical-documentation/data-sheets/4260fc.pdf
mfr: Analog Devices
mfr_pn: LTC4260
prefix: ltc4260
bus: i2c 
i2c_addr: 0x88


###########################################################################################################
#                                   REGISTERS                                                             #
###########################################################################################################

registers:
  - CONTROL:  { addr: 0x00, type: uint8_t, desc: Control Register, perm: RW, default: 0x1B }
  - ALERT:    { addr: 0x01, type: uint8_t, desc: Alert Register, perm: RW, default: 0x00 }
  - STATUS:   { addr: 0x02, type: uint8_t, desc: System status information, perm: R }
  - FAULT:    { addr: 0x03, type: uint8_t, desc: Fault Log, perm: RW }
  - SENSE:    { addr: 0x04, type: uint8_t, desc: ADC Current Sense Voltage Data, perm: RW}
  - SOURCE:   { addr: 0x05, type: uint8_t, desc: ADC Source votlage Data, perm: RW }
  - ADIN:     { addr: 0x06, type: uint16_t, desc: ADC ADIN Voltage Data, perm: RW }


###########################################################################################################
#                                   Fields                                                                #
###########################################################################################################

fields: 

  - CONTROL:
    - GPIO_CFG: 
        mask: 0xC
        desc: Configures behavior of GPIO pin
        values:
          - PWR_GOOD: { value: 0x0, desc: Power Good  }
          - PWR_BAD:  { value: 0x1, desc: Power Bad  }
          - OUTPUT:   { value: 0x2, desc: Output (Set by Alert:GPIO_OUT) }
          - INPUT:    { value: 0x3, desc: Input (Read at STATUS:GPIO_IN) }
    - TEST_MODE_EN:   {bit: 5, desc: Enables Test Mode. Halts ADC operation and enables writing to ADC registers}
    - MASS_WRITE_EN:  {bit: 4, desc: Enables writing to all devices using address b1011111}
    - FET_CTRL:       {bit: 3, desc: Turn FET ON}
    - OVERCURRENT_AUTORETRY: {bit: 2, desc: Enables automatic retry after overcurrent fault}
    - UNDERVOLTAGE_AUTORETRY: {bit: 1, desc: Enables automatic retry after undervoltage fault}
    - OVERVOLTAGE_AUTORETRY: {bit: 0, desc: Enables automatic retry after overvoltage fault}

  - ALERT: 
    - GPIO_OUT:           {bit: 6, desc: Output Data bit to GPIO Pin when configured as output}
    - FET_SHORT:          {bit: 5, desc: Enables Alert for FET Short condition}
    - BD_PRST_CHANGE:     {bit: 4, desc: Enables Alert when BD_PRST changes state}
    - PWR_BAD:            {bit: 3, desc: Enables Alert when output power is bad }
    - OVERCURRENT:        {bit: 2, desc: Enables Alert when overcurrent fault occurs}
    - UNDERVOLTAGE:       {bit: 1, desc: Enables Alert when undervoltage fault occurs}
    - OVERVOLTAGE:        {bit: 0, desc: Enables Alert when overvoltage fault occurs}
  
  - STATUS:
    - FET:          {bit: 7, desc: Indicates state of FET }
    - GPIO_IN:      {bit: 6, desc: State of GPIO pin when configured as input}
    - FET_SHORT:    {bit: 5, desc: Indicates Potential FET Short if Current Sense Voltage Exceeds 2mV While FET is Off }
    - BD_PRST:      {bit: 4, desc: Indicates if Board Present is Present}
    - PWR_BAD:      {bit: 3, desc: Indicates Power is Bad When FB is Low }
    - OVERCURRENT:  {bit: 2, desc: Indicates Overcurrent Condition During Cool Down Cycle}
    - UNDERVOLTAGE: {bit: 1, desc: Indicates Input Undervoltage When UV is Low}
    - OVERVOLTAGE:  {bit: 0, desc: Indicates Input Overvoltage When OV is High }

  - FAULT: 
    - FET_SHORT:           {bit: 5, desc:  Indicates Potential FET Short was Detected When Measured Current Sense Voltage Exceeded 2mV(code 0000111) While FET was Off }
    - BD_PRST_CHANGE:      {bit: 4, desc: Indicates that a Board was Inserted or Extracted When BD_PRST Changed State}
    - PWR_BAD:             {bit: 3, desc:  Indicates Power was Bad When FB Went Low (C3 = 1)}
    - OVERCURRENT:         {bit: 2, desc: Indicates Overcurrent Fault occured}
    - UNDERVOLTAGE:        {bit: 1, desc: Indicates Input Undervoltage Fault Occurred When UV Went Low}
    - OVERVOLTAGE:         {bit: 0, desc: Indicates Input Overvoltage Fault Occurred When OV Went High}
  
